#!/usr/bin/env bash

cargo install \
  cargo-edit \
  cargo-fix \
  cargo-make \
  cargo-testify \
  cargo-update \
  cargo-watch \
  pretty-assertions \
  rusty-hook
