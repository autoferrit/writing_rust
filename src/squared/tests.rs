#[cfg(test)]
use super::*;
use pretty_assertions::assert_eq;

#[test]
fn squared_test() {
    assert_eq!(25, squared(5));
}
