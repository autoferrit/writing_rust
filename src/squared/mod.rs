mod tests;

pub fn squared_main() {
    println!("Squared(5): {}", squared(5));
}

fn squared(n: i32) -> i32 {
    n * n
}
