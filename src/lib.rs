#[cfg(test)]
mod tests {
    use pretty_assertions::{assert_eq, assert_ne};

    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
        assert_ne!(1, 2)
    }
}
